all:
	echo "hcg-minus"

mk-cmd:
	echo "hcg-minus - NIL"

clean:
	rm -fR dist

push-all:
	r.gitlab-push.sh hcg-minus

push-tags:
	r.gitlab-push.sh hcg-minus --tags

indent:
	fourmolu -i Data

doctest:
	doctest -Wno-x-partial -Wno-incomplete-uni-patterns Data
