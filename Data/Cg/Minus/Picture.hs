-- | Very simple picture model.
module Data.Cg.Minus.Picture where

import Data.List {- base -}
import Data.Maybe {- base -}

import Music.Theory.Math {- hmt-base -}

import Data.Cg.Minus.Colour {- hcg-minus -}
import Data.Cg.Minus.Core {- hcg-minus -}
import Data.Cg.Minus.Types {- hcg-minus -}

-- * Type

type Line_Width r = r

-- | (dash,no-dash)
type Dash r = ([r], r)

no_dash :: Num r => Dash r
no_dash = ([], 0)

-- | (line-width,colour,dash-pattern)
data Pen r = Pen (Line_Width r) Ca (Dash r) deriving (Eq, Show)

-- | (centre,radius)
type Centre_Radius t = (Pt t, t)

data Mark r
  = Line (Pen r) (Ln r)
  | Polygon (Either (Pen r) Ca) [Pt r]
  | Circle (Either (Pen r) Ca) (Centre_Radius r)
  | Dot Ca (Centre_Radius r)
  deriving (Eq, Show)

type Picture r = [Mark r]

-- * Constructors

line_seq :: Num r => Pen r -> [Pt r] -> [Mark r]
line_seq pen =
  let adj l = zip l (tail l)
  in map (Line pen . uncurry Ln) . adj

polygon_l :: Pen r -> [Pt r] -> Mark r
polygon_l pen = Polygon (Left pen)

polygon_f :: Ca -> [Pt r] -> Mark r
polygon_f clr = Polygon (Right clr)

circle_l :: Pen r -> Centre_Radius r -> Mark r
circle_l pen = Circle (Left pen)

circle_f :: Ca -> Centre_Radius r -> Mark r
circle_f clr = Circle (Right clr)

-- * Analysis

mark_wn :: (Num n, Ord n) => Mark n -> Wn n
mark_wn m =
  case m of
    Line _ ln -> ln_wn ln
    Polygon _ p -> pts_window p
    Circle _ (c, r) -> wn_square c r
    Dot _ (c, r) -> wn_square c r

mark_normal :: Ord r => Mark r -> Mark r
mark_normal m =
  case m of
    Line p ln -> Line p (ln_sort ln)
    Polygon _ _ -> m -- should ensure CCW
    Circle _ _ -> m
    Dot _ _ -> m

mark_pt_set :: Mark r -> [Pt r]
mark_pt_set m =
  case m of
    Line _ (Ln p q) -> [p, q]
    Polygon _ p -> p
    Circle _ (p, _) -> [p]
    Dot _ (p, _) -> [p]

mark_ln :: Mark r -> Maybe (Ln r)
mark_ln m =
  case m of
    Line _ l -> Just l
    _ -> Nothing

mark_circle :: Mark r -> Maybe (Centre_Radius r)
mark_circle m =
  case m of
    Circle _ c -> Just c
    _ -> Nothing

picture_pt_set :: Picture r -> [Pt r]
picture_pt_set = concatMap mark_pt_set

picture_ln_set :: Picture r -> [Ln r]
picture_ln_set = mapMaybe mark_ln

picture_ln_intersections :: (Fractional r, Ord r) => Picture r -> [Pt r]
picture_ln_intersections p =
  let l = picture_ln_set p
  in catMaybes [ln_intersection l0 l1 | l0 <- l, l1 <- l, l0 /= l1]

-- | nep = no equal points, ie. lines do not share an endpoint.
picture_ln_intersections_nep :: (Fractional r, Ord r) => Picture r -> [Pt r]
picture_ln_intersections_nep p =
  let l = picture_ln_set p
      f (Ln p1 p2) (Ln p3 p4) = nub [p1, p2, p3, p4] == [p1, p2, p3, p4]
  in catMaybes [ln_intersection l0 l1 | l0 <- l, l1 <- l, f l0 l1]

picture_ln_circle_intersections :: (Floating r, Ord r) => Picture r -> [Pt r]
picture_ln_circle_intersections p =
  let l_set = picture_ln_set p
      c_set = mapMaybe mark_circle p
  in concat [ln_circle_intersection_set l c | l <- l_set, c <- c_set]

picture_normalise :: Ord r => Picture r -> Picture r
picture_normalise = nub . map mark_normal

picture_wn :: (Ord r, Num r) => Picture r -> Wn r
picture_wn = foldl1 wn_join . map mark_wn

-- * Graph

{- | Extract coloured vertext-sequences from a picture.
     Dots and circles generate 1-element sequences, lines 2-element sequences, n-polygons generate n+1-element sequences.
-}
picture_ln :: Num c => Picture t -> [((c, c, c), [(t, t)])]
picture_ln mk =
  let un_c = c_to_rgb8 . pureColour
      get_c x = case x of
        Left (Pen _ c _) -> un_c c
        Right c -> un_c c
      f m = case m of
        Line (Pen _ c _) (Ln p1 p2) -> (un_c c, [p1, p2])
        Polygon c p -> (get_c c, p ++ [head p])
        Circle c (p1, _) -> (get_c c, [p1])
        Dot c (p1, _) -> (un_c c, [p1])
  in map (fmap (map pt_xy) . f) mk

{- | Extract graph from set of coloured vertex sequences, ie. 'picture_ln'.
     Vertices are compared using the given equality function (ie. '~=')
     Deletes duplicate vertices and edges, which are un-directed.
-}
picture_ln_gr :: (Ord n, Ord c) => (n -> n -> Bool) -> [(c, [n])] -> ([(Int, n)], [((Int, Int), c)])
picture_ln_gr eq_f ln =
  let v = nubBy eq_f (sort (concatMap snd ln))
      v_ix x = fromMaybe (error "picture_ln_gr?") (findIndex (eq_f x) v)
      o (i, j) = (min i j, max i j)
      adj2 x = zip x (tail x)
      mk_e (c, p) = map (\(i, j) -> (o (v_ix i, v_ix j), c)) (adj2 p)
      e = nub (sort (concatMap mk_e ln))
  in (zip [0 ..] v, e)

-- | 'picture_ln_gr' of 'picture_ln' of '~='
picture_gr :: (Floating n, Ord n, Num c, Ord c) => Picture n -> ([(Int, (n, n))], [((Int, Int), (c, c, c))])
picture_gr =
  let eq (i, j) (p, q) = i ~= p && j ~= q
  in picture_ln_gr eq . picture_ln
