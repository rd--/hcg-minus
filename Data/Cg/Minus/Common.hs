module Data.Cg.Minus.Common where

-- * Ord

{- | Given /left/ and /right/, is /x/ in range (inclusive).

>>> map (in_range 0 1) [-1,0,1,2]
[False,True,True,False]
-}
in_range :: Ord a => a -> a -> a -> Bool
in_range l r x = l <= x && x <= r

-- * List

{- | Split list at element where predicate /f/ over adjacent elements first holds.

>>> split_f (\p q -> q - p < 3) [1,2,4,7,11]
([1,2,4],[7,11])
-}
split_f :: (a -> a -> Bool) -> [a] -> ([a], [a])
split_f f =
  let go i [] = (reverse i, [])
      go i [p] = (reverse (p : i), [])
      go i (p : q : r) =
        if f p q
          then go (p : i) (q : r)
          else (reverse (p : i), q : r)
  in go []

{- | Variant on 'split_f' that segments input.

>>> segment_f (\p q -> abs (q - p) < 3) [1,3,7,9,15]
[[1,3],[7,9],[15]]
-}
segment_f :: (a -> a -> Bool) -> [a] -> [[a]]
segment_f f xs =
  let (p, q) = split_f f xs
  in if null q
      then [p]
      else p : segment_f f q

{- | Delete elements of a list using a predicate over the
previous and current elements.
-}
delete_f :: (a -> a -> Bool) -> [a] -> [a]
delete_f f =
  let go [] = []
      go [p] = [p]
      go (p : q : r) =
        if f p q
          then go (p : r)
          else p : go (q : r)
  in go

{- | All adjacent pairs of a list.

>>> pairs [1..5]
[(1,2),(2,3),(3,4),(4,5)]
-}
pairs :: [x] -> [(x, x)]
pairs l =
  case l of
    x : y : z -> (x, y) : pairs (y : z)
    _ -> []

-- * Bifunctor

bimap1 :: (c -> d) -> (c, c) -> (d, d)
bimap1 f (p, q) = (f p, f q)
