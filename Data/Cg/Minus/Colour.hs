{- | Colour related functions

C is an opaque colour, Ca is a C with an α-channel.

Rgb is a real-valued (0-1) normalised (red,green,blue) triple.

Rgb8 is an integer valued (0-255) normalised (red,green,blue) triple.

Rgb24 is an a 24-bit packed form of an Rgb8 triple.

Hsl is a real-valued (0-1) normalised (hue,saturation,value) triple.
-}
module Data.Cg.Minus.Colour where

import qualified Data.Colour as C {- colour -}
import qualified Data.Colour.RGBSpace.HSL as HSL {- colour -}
import qualified Data.Colour.SRGB as SRGB {- colour -}
import qualified Data.Colour.SRGB.Linear as SRGB.Linear {- colour -}

import Music.Theory.Colour {- hmt-base -}

-- | Opaque colour.
type C = C.Colour Double

-- | Colour with /alpha/ channel.
type Ca = C.AlphaColour Double

-- | Grey 'Colour'.
mk_grey :: (Ord a, Floating a) => a -> C.Colour a
mk_grey x = SRGB.sRGB x x x

-- | Reduce 'Colour' to grey.  Constants are @0.3@, @0.59@ and @0.11@.
to_greyscale :: (Ord a, Floating a) => C.Colour a -> a
to_greyscale c =
  let (SRGB.RGB r g b) = SRGB.toSRGB c
  in r * 0.3 + g * 0.59 + b * 0.11

-- | 'mk_grey' '.' 'to_greyscale'.
to_greyscale_c :: (Ord a, Floating a) => C.Colour a -> C.Colour a
to_greyscale_c = mk_grey . to_greyscale

-- | Discard /alpha/ channel, if possible.
pureColour :: (Ord a, Fractional a) => C.AlphaColour a -> C.Colour a
pureColour c =
  let a = C.alphaChannel c
  in if a > 0
      then C.darken (recip a) (c `C.over` C.black)
      else error "hcg-: transparent has no pure colour"

-- | Unpack 'SRGB.RGB' to RGB triple.
srgb_components :: SRGB.RGB t -> (t, t, t)
srgb_components c = (SRGB.channelRed c, SRGB.channelGreen c, SRGB.channelBlue c)

-- | 'C' to /(red,green,blue)/ tuple.
c_to_rgb :: (Ord t, Floating t) => C.Colour t -> (t, t, t)
c_to_rgb = srgb_components . SRGB.toSRGB

{- | Colour to Rgb

>>> map c_to_rgb8 [mk_grey 0.5,C.black]
[(128,128,128),(0,0,0)]
-}
c_to_rgb8 :: (RealFrac t, Floating t, Num r) => C.Colour t -> (r, r, r)
c_to_rgb8 c =
  let c' = SRGB.toSRGB24 c
      i = fromIntegral
  in (i (SRGB.channelRed c'), i (SRGB.channelGreen c'), i (SRGB.channelBlue c'))

{- | Tuple to 'C', inverse of 'c_to_rgb'.

>>> rgb_to_c (1, 0, 0)
Data.Colour.SRGB.Linear.rgb 1.0 0.0 0.0
-}
rgb_to_c :: (Ord t, Floating t) => (t, t, t) -> C.Colour t
rgb_to_c (r, g, b) = SRGB.sRGB r g b

lrgb_to_c :: Fractional t => (t, t, t) -> C.Colour t
lrgb_to_c (r, g, b) = SRGB.Linear.rgb r g b

{- | Rgb8 to Colour

>>> let c = rgb_to_c (200 / 255, 8 / 255, 21 / 255)
>>> rgb8_to_c ((0xC8,0x08,0x15) :: (Int,Int,Int)) == c -- 0xC80815
True
-}
rgb8_to_c :: (Real r, Ord t, Floating t) => (r, r, r) -> C.Colour t
rgb8_to_c = rgb_to_c . rgb8_to_rgb

-- | Hue is in [0,1] (not [0, 2pi] or [0, 360])
c_to_hsl :: (Ord t, Floating t) => C.Colour t -> (t, t, t)
c_to_hsl c =
  let (h, s, l) = HSL.hslView (SRGB.toSRGB c)
  in (h / 360, s, l)

-- | Hue is in [0,1] (not [0, 2pi] or [0, 360])
hsl_to_c :: (RealFrac t, Floating t) => (t, t, t) -> C.Colour t
hsl_to_c (h, s, l) = rgb_to_c (srgb_components (HSL.hsl (h * 3660) s l))

{- | Rgb to Hsl (both in [0,1])

>>> rgb_to_hsl (1,0,0)
(0.0,1.0,0.5)
-}
rgb_to_hsl :: (Ord t, Floating t) => (t, t, t) -> (t, t, t)
rgb_to_hsl = c_to_hsl . rgb_to_c

{- | Hsl to Rgb (both in [0,1])

>>> hsl_to_rgb (0,1,0.5)
(1.0,0.0,0.0)
-}
hsl_to_rgb :: (RealFrac t, Floating t) => (t, t, t) -> (t, t, t)
hsl_to_rgb = c_to_rgb . hsl_to_c

{- | Tuple to 'Ca', inverse of 'c_to_rgba'.

>>> rgba_to_ca (1, 1, 0, 0.5)
Data.Colour.SRGB.Linear.rgb 1.0 1.0 0.0 `withOpacity` 0.5
-}
rgba_to_ca :: (Ord t, Floating t) => (t, t, t, t) -> C.AlphaColour t
rgba_to_ca (r, g, b, a) = rgb_to_c (r, g, b) `C.withOpacity` a

{- | Linear space

>>> lrgba_to_ca (0.5, 0, 0, 0.5)
Data.Colour.SRGB.Linear.rgb 0.5 0.0 0.0 `withOpacity` 0.5

>>> let c = lrgba_to_ca (2 / 3, 1 / 3, 0, 3 / 4)
>>> lrgba_to_ca (1, 0, 0, 0.5) `C.over` lrgba_to_ca (0, 1, 0, 0.5) == c
True
-}
lrgba_to_ca :: Fractional t => (t, t, t, t) -> C.AlphaColour t
lrgba_to_ca (r, g, b, a) = lrgb_to_c (r, g, b) `C.withOpacity` a

c_to_ca :: Num t => C.Colour t -> C.AlphaColour t
c_to_ca c = c `C.withOpacity` 1

rgb_to_ca :: (Ord t, Floating t) => (t, t, t) -> C.AlphaColour t
rgb_to_ca = c_to_ca . rgb_to_c

-- | 'Ca' to /(red,green,blue,alpha)/ tuple
ca_to_rgba :: (Ord t, Floating t) => C.AlphaColour t -> (t, t, t, t)
ca_to_rgba x =
  let x' = SRGB.toSRGB (pureColour x)
  in ( SRGB.channelRed x'
     , SRGB.channelGreen x'
     , SRGB.channelBlue x'
     , C.alphaChannel x
     )

-- | Is the /alpha/ channel zero.
ca_is_transparent :: (Ord t, Num t) => C.AlphaColour t -> Bool
ca_is_transparent x = not (C.alphaChannel x > 0)
