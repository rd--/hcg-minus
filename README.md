hcg-minus
---------

a simple non-optimised [haskell](http://haskell.org/) cg library

© [rohan drape](http://rohandrape.net/), 2009-2024, [gpl](http://gnu.org/copyleft/)

* * *

```
$ make doctest
Examples: 154  Tried: 154  Errors: 0  Failures: 0
$
```
